require 'uri'
require 'yaml'
module NavigationHelpers
  extend self
  def path_to(page_name)
    config = YAML::load(open(File.join(File.dirname(__FILE__),'config.yml')))
    URI::join("http://#{config['WEBHOST']}/", page_name)
  end
end
World(NavigationHelpers)
