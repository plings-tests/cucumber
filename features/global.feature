Feature: Yahoo Recognition
  In order that Yahoo knows this is our website
  The index page should have an appropriate meta tag
  
  Scenario: Test to see if we have the Yahoo meta tag on the page
	Given I am on /
	Then I should see "<meta name="y_key" content="e3a26b7624a54695" />"
