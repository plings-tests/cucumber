Feature: Top Navigation
  In order that people can easily navigate our website
  The top navigation bar should be on every page and have useful links
  
  Scenario: Test to see if we have the 'things to do' link
	Given I am on /
	Then I should see "<a title="Things To Do" href="/thingstodo">Things to do</a>"
	#And I should see "<a title="About Plings" href="http://about.plings.net/about">About</a>"
	And I should see "<a href="http://www.plings.net/blog">Blog</a>"
	#And I should see "<a title="Adding Plings" href="http://about.plings.net/adding-plings">Add</a>"
	And I should see "<a title="Get Plings your way" href="/about/apps">Apps</a>"
	And I should see "<a title="News" href="/about/featured-plings">News</a>"
	And I should see "<a title="Search Plings" href="/browse">Search</a>"
	


