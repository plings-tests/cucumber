Feature: Transport Direct Inteligent Link
  In order to help people get travel directions to an activity venue
  I want to display an inteligent link to the Transport Direct website that sets the venue as my destination
  
  Scenario Outline: Test to see if we have the correct text to introduce our link to Transport direct
	Given I am on /a/<activity_id>
	Then I should see a link to "http://www.transportdirect.info/web2/journeyplanning/jpLandingPage.aspx?id=bgsite&d=344150,392250&dn=St+George%27s+Church+Hall%2C+Liverpool%2C+L36+8BE" with text "Public Transport (TransportDirect)"
	And I should see a "transport" section with the text "Get directions to"
	And I should see a "transport" section with the text "Cycling (CycleStreets)"
  Examples:
  |activity_id|
  |642492|
