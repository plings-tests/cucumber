Feature: Venues
  Anyone should be able to useful information about a venue when viewing any of the activities
  
  Scenario Outline: Check a venue name is correct
	Given I am on /a/<activity_id>
	Then I should see "<venue_name>"
	And I should not see "<venue_name_wrong>"
  Examples:
  |activity_id|venue_name|venue_name_wrong|
  |642492|St George's Church Hall|St George''s Church Hall|
