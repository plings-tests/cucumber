Feature: Organisations
  Anyone should be able to useful information about an organisation when viewing any of the organisation pages
  
  Scenario Outline: Check external urls have google analytics external link javascript
	Given I am on /o/<org_id>
	Then I should see "onClick="recordOutboundLink"
  Examples:
  |org_id|
  |6565|
