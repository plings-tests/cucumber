require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "paths"))

Then /^I should see a link to "([^\"]*)" with text "([^\"]*)"$/ do |url, text|
  response_body.should have_selector("a[href='#{ url }']") do |element|
    element.should contain(text)
  end
end

Then /^I should see a "([^\"]*)" section with the text "([^\"]*)"$/ do |section,text|
  # checks for text within a specified div id
	response_body.should have_selector("div[id='#{ section }']") do |element|
    element.should contain(text)
  end  
end


