Feature: Paging
  In order that people can navigate through many results
  The search results pages should have a working pager
  
  Scenario: Test to see if the previous link is working
	Given I am on /la/00CJ?Page=2
	Then I should see "<a href="/la/00CJ?Page=1""
    And I should see "id="ctl00_cphMain_ucActivityList_lbtPrevious""
    And I should see ">&lt;</a>"
	And I should see "<a href="/la/00CJ?Page=3""
    And I should see "id="ctl00_cphMain_ucActivityList_lbtNext""
    And I should see ">&gt;</a>"


